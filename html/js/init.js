$(document).ready( function() {
    //main menu---------------------------
    // $('.menu-toggler').click(function(){
    //     $('.main-menu').stop().fadeIn();
    // });
    // $('.main-menu__close').click(function(){
    //     $('.main-menu').stop().fadeOut();
    // });
    class ToggleMenu {
        constructor() {
            this.menuToggler = $('.menu-toggler');
            this.menuClose = $('.main-menu__close');
            this.menuTarget = $('.main-menu');
            this.events();
        }
        events() {
            this.menuToggler.click(this.showMenu.bind(this));
            this.menuClose.click(this.hideMenu.bind(this));
        }
        showMenu() {
            this.menuTarget.stop().fadeIn();
        }
        hideMenu() {
            this.menuTarget.stop().fadeOut();
        }
    }
    var toggleMenu = new ToggleMenu();


    //------------------------------------

    $('.callback-block__toggler').click(function(){
        $(this).parent().find('.callback-block__form').toggleClass('active');
        $(this).find('.fa').toggleClass('fa-chevron-down').toggleClass('fa-chevron-up');
    });
    //------------------------------------
    // slick sliders
    $('#home-slider').slick({
        dots: true,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 2500,
        speed: 1000,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: $(".home-slider__arrows .slick-prev"),
        nextArrow: $(".home-slider__arrows .slick-next")
    });

    $('#offers-slider').slick({
        dots: false,
        infinite: false,
        autoplay: true,
        autoplaySpeed: 2500,
        speed: 1000,
        slidesToShow: 1,
        slidesToScroll: 1,
        draggable: false,
        focusOnSelect: true,
        prevArrow: $(".offers-slider__arrows .slick-prev"),
        nextArrow: $(".offers-slider__arrows .slick-next")
    });

    $('.page-price__slider').slick({
        dots: false,
        infinite: true,
        autoplay: false,
        autoplaySpeed: 2500,
        speed: 1000,
        slidesToShow: 3,
        slidesToScroll: 1,
        prevArrow: $(".page-price__slider__arrows .slick-prev"),
        nextArrow: $(".page-price__slider__arrows .slick-next"),
        responsive: [
            {
              breakpoint: 1200,
              settings: {
                slidesToShow: 2
              }
            },
            {
              breakpoint: 992,
              settings: {
                slidesToShow: 1
              }
            },
            {
              breakpoint: 576,
              settings: {
                slidesToShow: 1
              }
            }
          ]
    });

     // Initialize
    //  var bLazy = new Blazy({ 
    //     // container: '#home-slider' // Default is window
    // });
    // $('#home-slider').on('afterChange', function(event, slick, direction){
    //     bLazy.revalidate();
    // });
    // $('#offers-slider').on('afterChange', function(event, slick, direction){
    //     bLazy.revalidate();
    // });

    $('#reviews-slider').slick({
        dots: true,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 2500,
        speed: 1000,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: $(".reviews-slider__arrows .slick-prev"),
        nextArrow: $(".reviews-slider__arrows .slick-next")
    });
});


